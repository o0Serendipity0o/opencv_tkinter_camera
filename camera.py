import tkinter as tk
from PIL import Image, ImageTk
import cv2
import threading


class MainWindow():
    def __init__(self, window):
        self.window = window

        self.cap = cv2.VideoCapture(0)
        self.width = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        global is_color
        is_color = True
        self.cap.set(cv2.CAP_PROP_EXPOSURE, 0)
        self.cap.set(cv2.CAP_PROP_BRIGHTNESS, 0)
        # parameter to set up with tk widget
        # self.gray = cv2.COLOR_BGR2GRAY
        # self.color = cv2.COLOR_BGR2RGB
        # self.cap.set(cv2.CAP_PROP_BRIGHTNESS, 0)
        # self.cap.set(cv2.CAP_PROP_SATURATION, 0)
        # self.cap.set(cv2.CAP_PROP_GAIN, 0)
        self.interval = 5
        # tkinter wigdets
        self.video_canvas = tk.Canvas(
            self.window, width=self.width, height=self.height)
        self.video_canvas.grid(row=0, column=0, columnspan=4, rowspan=4)
        self.start_button = tk.Button(self.window, text='Start video')
        self.start_button['command'] = self.start_video
        self.start_button.grid(row=5, column=0)
        self.color_button = tk.Button(self.window, text='color')
        self.color_button.grid(row=5, column=1)
        self.color_button['command'] = self.update_button_text
        self.exposure_time_label = tk.Label(self.window, text='exposure time')
        self.exposure_time_label.grid(row=0, column=5, padx=5, pady=5, sticky='s')
        self.exposure_time = tk.Spinbox(self.window, from_=0, to=100)
        self.exposure_time.grid(row=1, column=5, padx=5, pady=5, sticky='n')
        self.brightness_label = tk.Label(self.window, text='brightness')
        self.brightness_label.grid(row=2, column=5, padx=5, pady=5, sticky='s')
        self.brightness_Spinbox = tk.Spinbox(self.window, from_=0, to=500)
        self.brightness_Spinbox.grid(row=3, column=5, padx=5, pady=5, sticky='n')

    def start_video(self):
        thread = threading.Thread(target=self.update_image)
        thread.start()
        # thread.join()

    def update_image(self):
        self.exp_time()
        self.bright()
        ret, frame = self.cap.read()
        color = self.change_color()
        self.image = cv2.cvtColor(frame, color)
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.video_canvas.create_image(0, 0, anchor=tk.NW, image=self.image)
        self.window.after(self.interval, self.update_image)

    def update_button_text(self):
        global is_color
        if is_color:
            is_color = False
            self.color_button['text'] = 'gray'
        else:
            is_color = True
            self.color_button['text'] = 'color'

    def change_color(self):
        global is_color
        if is_color:
            self.color = cv2.COLOR_BGR2GRAY
        else:
            self.color = cv2.COLOR_BGR2RGB
        return self.color

    def exp_time(self):
        value = self.exposure_time.get()
        exp = self.cap.set(cv2.CAP_PROP_EXPOSURE, int(value))
        return exp

    def bright(self):
        value = self.brightness_Spinbox.get()
        self.cap.set(cv2.CAP_PROP_BRIGHTNESS, int(value))
        return value


def main():
    root = tk.Tk()
    MainWindow(root)
    root.mainloop()


if __name__ == "__main__":
    main()
